const express = require('express');
const app = express();
const cors = require('cors') // modulo para la facil integracion entre el servidor de angular y node

app.use(cors());
app.use(express.json());
app.use('/api', require('./routes/routes'))
app.set('port',process.env.PORT || 3300);
app.listen(app.get('port'));
console.log('*******************************************************')
console.log('Starting and gathering information on -> [\x1b[32mCASPER\x1b[0m] ...')
console.log('[\x1b[32mCASPER\x1b[0m] -> \x1b[33mactive\x1b[0m -> \x1b[31m%s\x1b[0m', app.get('port'))